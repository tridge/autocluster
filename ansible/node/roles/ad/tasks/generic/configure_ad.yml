---
- name: check if AD server active flag file exists
  stat:
    path: /root/.autocluster/ad_active
  register: ad_active

- name: remove smb.conf
  vars:
    tdir: "{{ tarball_install_prefix }}/etc"
    pdir: /etc/samba
    samba_config_dir: "{{ tdir if tarball is not none else pdir }}"
  file:
    path: "{{ samba_config_dir }}/smb.conf"
    state: absent
  when: not ad_active.stat.exists

- name: provision domain
  command: >
    samba-tool domain provision
      --server-role="dc"
      --use-rfc2307
      --dns-backend="SAMBA_INTERNAL"
      --realm="{{ resolv_conf.domain }}"
      --domain="{{ samba.workgroup }}"
      --adminpass="{{ ad.admin_password }}"
      --host-ip={{ nodes[ansible_hostname].ips[0] }}
      --option="dns forwarder = {{ ad.dns_forwarder }}"
  when: not ad_active.stat.exists

- name: add users and groups
  command: samba-tool {{ p }}
  with_list:
    - domain passwordsettings set --min-pwd-length=3
    - domain passwordsettings set --complexity=off
    - user setexpiry --noexpiry Administrator
    - user setpassword administrator --newpassword="{{ ad.admin_password }}"
    - group add group1
    - group add group2
    - user add user1 "{{ ad.admin_password }}"
    - group addmembers group1 user1
    - user setexpiry --noexpiry user1
    - user add user2 "{{ ad.admin_password }}"
    - group addmembers group2 user2
    - user setexpiry --noexpiry user2
  loop_control:
    loop_var: p
  when: not ad_active.stat.exists

# This is created from a template in common/.  It might be good not to
# update this twice but we probably want a working configuration under
# the DC is started below.
- name: update /etc/resolv.conf
  lineinfile:
    path: /etc/resolv.conf
    regexp: "^nameserver.*"
    line: "nameserver {{ nodes[ansible_hostname].ips[0] }}"
  when: not ad_active.stat.exists

- name: ensure domain controller is enabled and running
  service:
    name: samba
    state: started
    enabled: yes
  when: not ad_active.stat.exists

- name: flag AD server as active
  file:
    path: /root/.autocluster/ad_active
    state: touch
