#!/bin/sh

for ip ; do
	out=$(ip addr show to "$ip")

	if [ -z "$out" ] ; then
		echo "ERROR: ${ip} was not found on an interface"
		ip addr
		exit 1
	fi
done
