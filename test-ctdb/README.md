What is this?
-------------

This is a wrapper script around autocluster and some configuration
fragments to build and test CTDB from some specified Samba git
branches.

How do I use this?
------------------

1. Create `test-repos.yml`

   This should contain an autocluster repositories list needed to
   install non-standard packages when building clusters.  This file
   can be empty.

2. Create a `git/` subdirectory (or symlink)

   Populate this with any necessary remotes for the branches being
   tested.

3. Create a BRANCHES file

   This contains branch names, 1 per line, comment lines start with '#'.

4. Run `./test_ctdb.sh`.

Branches can also be specified on the command-line.  For example:

```
./test_ctdb.sh origin/master origin/v4-12-test
```

Dependencies
------------

There should be very few dependencies, since the host machine only
needs to be a tarball.  The most likely dependencies will relate to
DocBook.

On CentOS 7 the following were needed:

* docbook-dtds
* docbook-style-xsl
