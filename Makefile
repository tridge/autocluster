version    := $(shell git describe 2>/dev/null | sed -e 's|autocluster-||' -e 's|-|_|g')
rpm_top_dir = $(shell rpmbuild -E '%_topdir')
rpm_dir     = $(rpm_top_dir)/RPMS/noarch
tarball     = autocluster-$(version).tar.gz
rpm         = autocluster-$(version)-1.noarch.rpm

prefix	= /usr/local
datadir	= ${prefix}/share/autocluster
bindir	= ${prefix}/bin

datas	= COPYING defaults.yml ansible vagrant test-ctdb
hacks	= autocluster.hack
genpkg  = debian/changelog autocluster.spec ChangeLog

all: $(hacks)

autocluster.hack: autocluster.py
	sed -e "s|^INSTALL_DIR = .*|INSTALL_DIR = '$(datadir)'|" $< > $@

install: all
	mkdir -p $(DESTDIR)$(datadir)
	cp -a $(datas) $(DESTDIR)$(datadir)/
	mkdir -p $(DESTDIR)$(bindir)
	install -m 755 autocluster.hack $(DESTDIR)$(bindir)/autocluster
	ln -s \
		$(datadir)/test-ctdb/autocluster-test-ctdb.sh \
		$(DESTDIR)$(bindir)/autocluster-test-ctdb

debian/changelog: debian/changelog.in always
	sed -e "s/@@DATE@@/$$(date '+%a, %e %b %Y %T %z')/" -e "s/@@VERSION@@/$(version)/" $< > $@

autocluster.spec: autocluster.spec.in always
	sed -e "s/@@VERSION@@/$(version)/" $< > $@

ChangeLog: always
	git log > ChangeLog

dist: tarball

tarball: $(genpkg)
	t=autocluster-$(version) && \
	git archive --prefix=$${t}/ HEAD > $${t}.tar && \
	tar r --transform s@^@$${t}/@ -f $${t}.tar $(genpkg) && \
	gzip -f $${t}.tar

rpm: tarball
	rpmbuild -tb $(tarball)
	mv $(rpm_dir)/$(rpm) $(rpm)

clean:
	rm -f $(hacks) $(genpkg)

.PHONY: all install dist tarball rpm clean always
